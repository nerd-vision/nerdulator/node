FROM node:10

COPY src /home/node/app/src

COPY package.json /home/node/app/package.json
COPY node_modules /home/node/app/node_modules

WORKDIR /home/node/app

EXPOSE 3000

CMD ["npm", "start"]