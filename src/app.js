var express = require('express');
var logger = require('morgan');
var cors = require('cors');

var calculatorRouter = require('./routes/router.js');

var app = express();
app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/', calculatorRouter);

module.exports = app;
