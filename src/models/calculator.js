module.exports = {
    add: function (int1, int2) {
        let result = parseInt(int1) + parseInt(int2);
        return result;
    },
    subtract: function (int1, int2) {
        let result = parseInt(int2) - parseInt(int1);
        return result;
    },
    multiply: function (int1, int2) {
        let result = parseInt(int1) * parseInt(int2);
        return result;
    },
    divide: function (int1, int2) {
        let result = parseInt(int1) / parseInt(int2);
        return result;
    },
};