const express = require('express');
const router = express.Router();
const calculator = require('../models/calculator');

router.get('/add', function (req, res, next) {
    let int1 = req.query.int1;
    let int2 = req.query.int2;
    let result = calculator.add(int1, int2);

    send(res, int1, int2, result, '+');
});

router.get('/subtract', function (req, res, next) {
    let int1 = req.query.int1;
    let int2 = req.query.int2;
    let result = calculator.subtract(int1, int2);

    send(res, int1, int2, result, '-');
});

router.get('/multiply', function (req, res, next) {
    let int1 = req.query.int1;
    let int2 = req.query.int2;
    let result = calculator.multiply(int1, int2);

    send(res, int1, int2, result, '*');
});

router.get('/divide', function (req, res, next) {
    let int1 = req.query.int1;
    let int2 = req.query.int2;
    let result = calculator.divide(int1, int2);

    send(res, int1, int2, result, '/');
});

function send(res, int1, int2, result, operator) {
    res.send(
        {
            'int1': int1,
            'int2': int2,
            'operation': operator,
            'result': result,
            'equation': int1 + operator + int2
        });
}

module.exports = router;
