# Nerdulator - Node

This is the node implementation of the nerdulator nerd.vision calculator demo API.

## Build

First run:

```bash
npm install
```

then build the Dockerfile

```bash
docker build -t nerdulator/node .
```

## Run

To run the API use docker-compose

```bash
docker-compose up
```

## Configure

To configure the agent, change the NV_API_KEY key in the docker-compose.
